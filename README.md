# Description
This is a collection of two ways to generate the FASER trigger LUT.

# Setup
This assumes you are working with the docker image `gitlab-registry.cern.ch/faser/docker/daq:master`.
Once booting up this image, you can build the code as
```
mkdir build
cd build
cmake3 ../
make -j8
```
which will produce an executable `mycode/makeLut`

# Usage
You can then build a LUT using either the python script or the executable you just build.
Note the difference in the logic symbols that are used in the calls to each.  For
the python-based execution, you will use AND (`&`) and OR (`|`) while for c++ you use
two of each, AND (`&&`) and OR (`||`).

## Python Based
```
python MakeLUT.py -o myLUT.txt -t0 "Tin0&Tin1" -t1 "(Tin2|Tin3)&(Tin4&notTin7)" -t2 "Tin3" -t3 "Tin4"
```


## c++ Based
```
cd build
./mycode/makeLut myLUT.txt "Tin0&&Tin1" "(Tin2||Tin3)&&(Tin4&&notTin7)" "Tin3" "Tin4"
```
and you can also use the nomenclature for triggers that is a bit more related to the 
experiment in this case
  - `Tin0` : (`FirstVetoLayer`) first veto layer
  - `Tin1` : (`SecondVetoLayer`) second veto layer
  - `Tin2` : (`TimingLayerTop`) timing layer top 
  - `Tin3` : (`TimingLayerBottom`) timing layer bottom 
  - `Tin4` : (`PreshowerLayer`) preshower layer 
  - `Tin5` : (`CalorimeterBottom`) calorimeter bottom 
  - `Tin6` : (`CalorimeterTop`) calorimeter top 
  - `Tin7` : (`Unused`) Unused 
so the above call would instead be
```
cd build
./mycode/makeLut myLUT.txt "FirstVetoLayer&&SecondVetoLayer" "(TimingLayerTop||TimingLayerBottom)&&(PreshowerLayer&&notUnused)" "TimingLayerBottom" "PreshowerLayer"
```