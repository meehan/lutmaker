// need to 
// $ yum install -y muParser

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <fstream>

#include "muParser.h"



void GenerateLUT(std::vector<uint16_t>& lut, std::vector<std::string> configs, std::map<std::string, std::string> trig_bit_map);
void TranslateConfigs(std::vector<std::string>& configs, std::map<std::string, std::string> trig_bit_map);
void VerifyConfigs(std::vector<std::string> configs, std::map<std::string, std::string> trig_bit_map);

std::string ConvertIntToWord(unsigned int word, bool debug=false);
void replaceAll(std::string& str, std::string from, std::string to);

int main(int argc, char *argv[])
{
  
  // get the output file name as the first argument
  std::string outfile;
  if(argc>1){
    outfile = argv[1];
  }
  
  // get the config triggers as the second name
  std::vector<std::string> configs;
  if(argc>2){
    for(int icon=2; icon<argc; icon++){
      configs.push_back(argv[icon]);
    }
  }
  else{
    configs.push_back("(notFirstVetoLayer&&notSecondVetoLayer)");
    configs.push_back("Tin1&&Tin2");
    configs.push_back("Unused&&Tin2");
    configs.push_back("Unused&&CalorimeterTop");
  }
  
  // hardcoded translation dictionary for physical to config space
  // should be in a config at some point
  std::map<std::string, std::string> trig_bit_map;
  trig_bit_map["Tin0"]="FirstVetoLayer";
  trig_bit_map["Tin1"]="SecondVetoLayer";
  trig_bit_map["Tin2"]="TimingLayerTop";
  trig_bit_map["Tin3"]="TimingLayerBottom";
  trig_bit_map["Tin4"]="PreshowerLayer";
  trig_bit_map["Tin5"]="CalorimeterBottom";
  trig_bit_map["Tin6"]="CalorimeterTop";
  trig_bit_map["Tin7"]="Unused";
  
  // this will be passed to the appropriate TLB access commane
  // https://gitlab.cern.ch/faser/gpiodrivers/-/blob/master/TLBAccess/src/TLBAccess.cxx#L569
  std::vector<uint16_t> lut;
  
  // create LUT from the configs
  GenerateLUT(lut, configs, trig_bit_map);
  
  std::cout<<"LUT size after : "<<lut.size()<<std::endl;
  
  // echo the LUT for verification
  std::cout<<"Writing output LUT"<<std::endl;
  std::ofstream fout;
  fout.open(outfile, std::ios::out);
  for(int itbp=0; itbp<(int)lut.size(); itbp++){
    std::cout<<std::setw(4)<<std::setfill('0')<<itbp<<"  "<<lut.at(itbp)<<std::endl;
    fout<<std::setw(4)<<std::setfill('0')<<lut.at(itbp)<<std::endl;
  }
  fout.close();
  
  return 0; 
}


  
void GenerateLUT(std::vector<uint16_t>& lut, std::vector<std::string> configs, std::map<std::string, std::string> trig_bit_map){
  
  for(int icon; icon<(int)configs.size(); icon++){
    std::cout<<"Initial Config "<<icon<<" : "<<configs.at(icon)<<std::endl;
  }
  
  TranslateConfigs(configs, trig_bit_map);
  
  for(int icon; icon<(int)configs.size(); icon++){
    std::cout<<"Readable Config "<<icon<<" : "<<configs.at(icon)<<std::endl;
  }
  
  VerifyConfigs(configs, trig_bit_map);
  
  double Tin[8]    = {0,0,0,0,0,0,0,0};
  double notTin[8] = {0,0,0,0,0,0,0,0};
  
  mu::Parser p;
  
  lut.clear();
  
  for(int inp=0; inp<256; inp++){
  
    // get the appropriate bitmask representation of the slot
    // and its inverse saved in Tin and notTin
    for(int ibit=0; ibit<8; ibit++){
      int val=0;
      if(inp & (1<<ibit)){
        val=1;
      }
          
      Tin[ibit]    = val;
      notTin[ibit] = 1-val;        
      
      p.DefineVar("Tin"+std::to_string(ibit),&Tin[ibit]);
      p.DefineVar("notTin"+std::to_string(ibit),&notTin[ibit]);
    }
    
    std::cout << ConvertIntToWord(inp) << std::endl;
  
    std::string tbp = "";
    
    for(int icon=0; icon<(int)configs.size(); icon++){
      p.SetExpr(configs.at(icon));
      int tbp_bit;
      try{
        tbp_bit = (int)p.Eval();
      }
      catch (mu::Parser::exception_type &e){
        std::cout << "Not possible to parse config : " << configs.at(icon) << std::endl;
        std::cout << e.GetMsg() << std::endl;
      }
      tbp = std::to_string(tbp_bit) + tbp;
    }
    
    std::cout<<"tbp : "<<tbp<<std::endl;
    
    lut.push_back(std::stoi(tbp));
  }
  
  
  std::cout<<"LUT size : "<<lut.size()<<std::endl;

}

void TranslateConfigs(std::vector<std::string>& configs, std::map<std::string, std::string> trig_bit_map){
  std::cout<<"TranslateLUT"<<std::endl;
  
  for (auto& [key, value]: trig_bit_map) {
    for(int icon=0; icon<(int)configs.size(); icon++){
      std::string key_str = (std::string)key;
      std::string value_str = (std::string)value;
      replaceAll(configs.at(icon), value_str, key_str);
    }    
  }
}

void VerifyConfigs(std::vector<std::string> configs, std::map<std::string, std::string> trig_bit_map){
  std::cout<<"VerifyConfigs"<<std::endl;

  for(int icon=0; icon<(int)configs.size(); icon++){
  
    std::string parse_config = configs.at(icon);
  
    // remove all of the logical variables 
    for (auto& [key, value]: trig_bit_map) {
      replaceAll(parse_config, value, " ");
      replaceAll(parse_config, key, " ");
      replaceAll(parse_config, "not", " ");
    }    
    
    // look for even number of &
    size_t n_ampersand = std::count(parse_config.begin(), parse_config.end(), '&');
    if(n_ampersand%2==1){
      std::cout<<"Improper expression of logic with &"<<std::endl;
    }
    replaceAll(parse_config, "&", "");
    
    // look for even number of |
    size_t n_bar = std::count(parse_config.begin(), parse_config.end(), '|');
    if(n_bar%2==1){
      std::cout<<"Improper expression of logic with |"<<std::endl;
    }    
    replaceAll(parse_config, "|", "");
    
     // look for equal number of occurences of parentheses
    size_t n_paren_left  = std::count(parse_config.begin(), parse_config.end(), '(');
    size_t n_paren_right = std::count(parse_config.begin(), parse_config.end(), ')');
    if(n_paren_left!=n_paren_left){
      std::cout<<"Improper balancing of parentheses"<<std::endl;
    }
    replaceAll(parse_config, "(", "");
    replaceAll(parse_config, ")", "");
    
    size_t n_space = std::count(parse_config.begin(), parse_config.end(), ' ');
    if(parse_config.size()!=n_space){
      std::cout<<"There is something wrong with the parsing of the trigger stream config : "<<configs.at(icon)<<std::endl;
    }
    
  }
}


std::string ConvertIntToWord(unsigned int word, bool debug){
  // converts an unsigned int to a string of 0's and 1's formatted
  // in an appropriate manner as an 8 bit word
  std::string wordout;

  for(int iBit=7; iBit>=0; iBit--){
    int bitval = (word & (1<<iBit)) >> iBit;
    wordout += std::to_string(bitval);

    if(iBit%4==0)
      wordout += " ";
  }

  return wordout;
}


void replaceAll(std::string& str, std::string from, std::string to) {
    if(from.empty())
        return;
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); 
    }
}

