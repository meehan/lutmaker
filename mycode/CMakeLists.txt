# CMakeLists files in this project can
# refer to the root source directory of the project as ${HELLO_SOURCE_DIR} and
# to the root binary directory of the project as ${HELLO_BINARY_DIR}.
cmake_minimum_required (VERSION 2.8.11)
project(samParser)

add_executable (makeLut makeLut.cpp)
target_include_directories(makeLut PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../muparser/include/)
target_link_libraries (makeLut LINK_PUBLIC muparser)